<?php

/**
 * @file
 */

/**
 * Implements hook_field_widget_info().
 */
function imagemapper_field_widget_info() {
  return array(
    'imagemapper_mapper' => array(
      'label' => t('Image Mapper'),
      'field types' => array('text_long'),
      'settings' => array(
        'image' => array(
          'uri' => '',
          'url' => '',
        ),
        'canvas' => array(
          'start' => '#000000',
          'main' => '#ff0000',
          'close' => '#0000ff',
        ),
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    )
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function imagemapper_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  $form['image'] = array(
    '#weight' => 15,
    '#tree' => TRUE,
    '#element_validate' => array('_imagemapper_settings_image_validate'),
  );
  $form['image']['uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Image'),
    '#default_value' => $settings['image'],
    '#required' => TRUE,
  );
  $form['image']['url'] = array(
    '#type' => 'value'
  );

  $form['canvas'] = array(
    '#type' => 'fieldset',
    '#title' => t('Canvas'),
    '#tree' => TRUE,
    '#weight' => 20,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['canvas']['start'] = array(
    '#type' => 'textfield',
    '#title' => t('Start point'),
    '#default_value' => $settings['canvas']['start'],
    '#required' => TRUE,
  );
  $form['canvas']['main'] = array(
    '#type' => 'textfield',
    '#title' => t('Line color (main)'),
    '#default_value' => $settings['canvas']['main'],
    '#required' => TRUE,
  );
  $form['canvas']['close'] = array(
    '#type' => 'textfield',
    '#title' => t('Line color (close)'),
    '#default_value' => $settings['canvas']['close'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 *
 */
function _imagemapper_settings_image_validate($element, &$form_state) {
  $uri = $element['uri']['#value'];

  if ($wrapper = file_stream_wrapper_get_instance_by_uri($uri)) {
    $url = $wrapper->getExternalUrl();

    form_set_value($element['url'], $url, $form_state);
  }
}

/**
 * Implements hook_field_widget_form().
 */
function imagemapper_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  $element += array(
    '#type' => 'fieldset',
  );

  $element['control'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="widget-imagemapper-' . $delta . '-control">',
    '#suffix' => '</div>',
  );
  $element['control']['clear'] = array(
    '#type' => 'button',
    '#value' => t('Clear'),
    '#attributes' => array(
      'class' => array(
        'widget-imagemapper-' . $delta . '-control-reset',
      ),
    ),
  );
  $element['control']['undo'] = array(
    '#type' => 'button',
    '#value' => t('Undo'),
    '#attributes' => array(
      'class' => array(
        'widget-imagemapper-' . $delta . '-control-undo',
      ),
    ),
  );

  $element['canvas'] = array(
    '#markup' => '<div class="widget-imagemapper-' . $delta . '-canvas"><canvas></canvas></div>',
  );

  $element['show'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show coordinates'),
    '#attributes' => array(
      'class' => array(
        'widget-imagemapper-' . $delta . '-show',
      ),
    ),
  );
  $element['value'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : '[]',
    '#attributes' => array(
      'class' => array(
        'widget-imagemapper-' . $delta . '-value',
      )
    ),
    '#states' => array(
      'invisible' => array(
        'input.widget-imagemapper-' . $delta . '-show' => array('checked' => FALSE),
      ),
    ),
  );
  $element['format'] = array(
    '#type' => 'value',
    '#value' => 1,
  );

  $element['#attributes'] = array(
    'id' => 'field-widget-imagemapper-' . $delta,
  );

  $element['#attached']['js'][] = drupal_get_path('module', 'imagemapper') . '/js/imagemapper.js';

  drupal_add_js(array('imageMapper' => array($delta => $settings)), 'setting');

  return array($element);
}
