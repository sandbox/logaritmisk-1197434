(function($) {

Drupal.behaviors.imageMapper = {
  attach: function(context, settings) {
    $.each(settings['imageMapper'], function(delta, config) {
      new Drupal.imageMapper(context, delta, config);
    });
  }
}

Drupal.imageMapper = function(context, delta, settings) {
  this._context = context;
  this._delta = delta;
  this._settings = settings;

  this._canvas = $('.widget-imagemapper-' + this._delta + '-canvas canvas', this._context).get(0);

  if (!this._canvas) {
    return;
  }

  this._context = this._canvas.getContext('2d');

  image = Image();
  image.src = this._settings['image']['url'];

  this._canvas.width = image.width;
  this._canvas.height = image.height;

  var _this = this;

  $(this._canvas)
    .css('background', 'url(' + this._settings['image']['url'] + ')')
    .bind('mousedown', function(event) {
      var o = $(event.currentTarget).offset();

      var x = event.pageX - Math.round(o.left);
      var y = event.pageY - Math.round(o.top);

      var coords = _this.get();

      coords.push({ x: x, y: y });

      _this.set(coords);

      _this.draw();
    });

  $('.widget-imagemapper-' + this._delta + '-control-reset')
    .bind('click', function(event) {
      event.preventDefault();

      _this.set([]);
      _this.draw();
    });

  $('.widget-imagemapper-' + this._delta + '-control-undo')
    .bind('click', function(event) {
      event.preventDefault();

      var coords = _this.get();

      if (coords.length) {
        coords.pop();

        _this.set(coords);
        _this.draw();
      }
    });

  this.draw();
}

Drupal.imageMapper.prototype.draw = function() {
  this.clear();

  var coords = this.get();

  if (coords.length <= 0) {
    return;
  }

  var _this = this;

  var coord = {};

  $.each(coords, function (i, c) {
    coord = c;

    if (i == 0) {
      _this._context.beginPath();
      _this._context.arc(c.x, c.y, 2, 0, Math.PI * 2, true);
      _this._context.closePath();
      _this._context.fill();

      _this._context.moveTo(c.x, c.y);
    }
    else {
      _this._context.lineTo(c.x, c.y);
    }
  });

  this._context.lineWidth = 1.5;
  this._context.lineCap = 'round';
  this._context.lineJoin = 'round';

  this._context.strokeStyle = "#ff0000";
  this._context.stroke();

  this._context.beginPath();
  this._context.moveTo(coord.x, coord.y);
  this._context.lineTo(coords[0].x, coords[0].y);

  this._context.strokeStyle = "#0000ff";
  this._context.stroke();
}

Drupal.imageMapper.prototype.clear = function() {
  this._context.clearRect(0, 0, this._canvas.width, this._canvas.height);
}

Drupal.imageMapper.prototype.set = function(value) {
  $('.widget-imagemapper-' + this._delta + '-value').val(JSON.stringify(value));
}
Drupal.imageMapper.prototype.get = function() {
  var value = $('.widget-imagemapper-' + this._delta + '-value').val();

  if (typeof value == 'string' && value != '') {
    try {
      return JSON.parse(value);
    }
    catch(e) {
    }
  }

  return [];
}

})(jQuery);
